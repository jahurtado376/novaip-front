import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProjectsRoutingModule } from './projects-routing.module';
import { ProjectsListComponent } from './projects-list/projects-list.component';
import { ProjectsViewComponent } from './projects-view/projects-view.component';
import { ProjectsCreateComponent } from './projects-create/projects-create.component';
import { ProjectsUpdateComponent } from './projects-update/projects-update.component';


@NgModule({
  declarations: [
    ProjectsListComponent,
    ProjectsViewComponent,
    ProjectsCreateComponent,
    ProjectsUpdateComponent
  ],
  imports: [
    CommonModule,
    ProjectsRoutingModule
  ]
})
export class ProjectsModule { }
