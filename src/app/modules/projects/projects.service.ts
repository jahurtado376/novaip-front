import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { createRequestParams } from 'src/app/utils/utils';
import { IProjects } from './projects';

@Injectable({
  providedIn: 'root'
})
export class ProjectsService {

  constructor(private http: HttpClient) { }

  public query(req?: any): Observable<IProjects[]>{
    let params = createRequestParams(req);
    return this.http.get<IProjects[]>(`${environment.END_POINT}/api/projects`, {params})
    .pipe(map(res => {
      return res;
    }));
  }

  public saveProjects(projects: IProjects): Observable<IProjects>{
    return this.http.post<IProjects>(`${environment.END_POINT}/api/projects`, projects)
    .pipe(map(res =>{
      return res;
    }));
  }

  public updateProjects(projects: IProjects): Observable<IProjects>{
    return this.http.put<IProjects>(`${environment.END_POINT}/api/projects`, projects)
    .pipe(map(res =>{
      return res;
    }));
  }

  public deleteProjects(id: string): Observable<IProjects>{
    return this.http.delete<IProjects>(`${environment.END_POINT}/api/projects/${id}`)
    .pipe(map(res =>{
      return res;
    }));
  }

  public getIdProjects(id: string): Observable<IProjects>{
    return this.http.get<IProjects>(`${environment.END_POINT}/api/projects/${id}`)
    .pipe(map(res =>{
      return res;
    }));    
  }
}
