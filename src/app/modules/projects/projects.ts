export interface IProjects {
    idProjects?: number;
    name?: string;
    description?: string;
    alias?: string;
    state?: boolean;
    deleted?: boolean;
    startDate?: Date;
    endDate?: Date;
    creationDate?: Date;
    updateDate?: Date;
}

export class Projects implements IProjects{
    constructor(
    public idProjects?: number,
    public name?: string,
    public description?: string,
    public alias?: string,
    public state?: boolean,
    public deleted?: boolean,
    public startDate?: Date,
    public endDate?: Date,
    public creationDate?: Date,
    public updateDate?: Date,
    ){}
}


