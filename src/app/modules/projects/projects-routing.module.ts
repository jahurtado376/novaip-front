import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProjectsCreateComponent } from './projects-create/projects-create.component';
import { ProjectsListComponent } from './projects-list/projects-list.component';
import { ProjectsUpdateComponent } from './projects-update/projects-update.component';
import { ProjectsViewComponent } from './projects-view/projects-view.component';

const routes: Routes = [
  {
    path: 'projects-list',
    component: ProjectsListComponent
  },
  {
    path: 'projects-create',
    component: ProjectsCreateComponent
  },
  {
    path: 'projects-view',
    component: ProjectsViewComponent
  },
  {
    path: 'projects-update',
    component: ProjectsUpdateComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjectsRoutingModule { }
