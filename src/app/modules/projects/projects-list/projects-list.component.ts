import { Component, OnInit } from '@angular/core';
import { IProjects } from '../projects';
import { ProjectsService } from '../projects.service';

@Component({
  selector: 'app-projects-list',
  templateUrl: './projects-list.component.html',
  styleUrls: ['./projects-list.component.styl']
})
export class ProjectsListComponent implements OnInit {

  projectsList: IProjects[] = [];

  pageSize = 5;
  pageNumber = 0;

  constructor(private projectsService: ProjectsService) { }

  ngOnInit(): void {
    this.projectsService.query({pageSize: this.pageSize,
      pageNumber: this.pageNumber})
    .subscribe((res: any) => {
      this.projectsList = res.content
      console.log('res', res);
    })
  }

}
