import { Component, OnInit } from '@angular/core';
import { IRols } from '../rols';
import { RolsService } from '../rols.service';

@Component({
  selector: 'app-rols-list',
  templateUrl: './rols-list.component.html',
  styleUrls: ['./rols-list.component.styl']
})
export class RolsListComponent implements OnInit {

  rolsList: IRols[] = [];

  pageSize = 5;
  pageNumber = 0;

  constructor(private rolsService: RolsService) { }

  ngOnInit(): void {
    this.rolsService.query({pageSize: this.pageSize,
      pageNumber: this.pageNumber})
    .subscribe((res: any) => {
      this.rolsList = res.content
      console.log('res', res);
    })
  }

}
