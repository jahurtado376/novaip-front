import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { createRequestParams } from 'src/app/utils/utils';
import { environment } from 'src/environments/environment';
import { IRols } from './rols';

@Injectable({
  providedIn: 'root'
})
export class RolsService {

  constructor(private http: HttpClient) { }

  public query(req?: any): Observable<IRols[]>{
    let params = createRequestParams(req);
    return this.http.get<IRols[]>(`${environment.END_POINT}/api/rols`, {params})
    .pipe(map(res => {
      return res;
    }));
  }

  public saveProjects(rols: IRols): Observable<IRols>{
    return this.http.post<IRols>(`${environment.END_POINT}/api/rols`, rols)
    .pipe(map(res =>{
      return res;
    }));
  }

  public updateProjects(rols: IRols): Observable<IRols>{
    return this.http.put<IRols>(`${environment.END_POINT}/api/rols`, rols)
    .pipe(map(res =>{
      return res;
    }));
  }

  public deleteProjects(id: string): Observable<IRols>{
    return this.http.delete<IRols>(`${environment.END_POINT}/api/projects/${id}`)
    .pipe(map(res =>{
      return res;
    }));
  }

  public getIdProjects(id: string): Observable<IRols>{
    return this.http.get<IRols>(`${environment.END_POINT}/api/rols/${id}`)
    .pipe(map(res =>{
      return res;
    }));    
  }
}
