import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RolsRoutingModule } from './rols-routing.module';
import { RolsViewComponent } from './rols-view/rols-view.component';
import { RolsCreateComponent } from './rols-create/rols-create.component';
import { RolsUpdateComponent } from './rols-update/rols-update.component';
import { RolsListComponent } from './rols-list/rols-list.component';


@NgModule({
  declarations: [
    RolsViewComponent,
    RolsCreateComponent,
    RolsUpdateComponent,
    RolsListComponent
  ],
  imports: [
    CommonModule,
    RolsRoutingModule
  ]
})
export class RolsModule { }
