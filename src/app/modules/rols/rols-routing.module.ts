import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RolsCreateComponent } from './rols-create/rols-create.component';
import { RolsListComponent } from './rols-list/rols-list.component';
import { RolsUpdateComponent } from './rols-update/rols-update.component';
import { RolsViewComponent } from './rols-view/rols-view.component';

const routes: Routes = [
  {
    path: 'rols-list',
    component: RolsListComponent
  },
  {
    path: 'rols-create',
    component: RolsCreateComponent
  },
  {
    path: 'rols-view',
    component: RolsViewComponent
  },
  {
    path: 'rols-update',
    component: RolsUpdateComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RolsRoutingModule { }
