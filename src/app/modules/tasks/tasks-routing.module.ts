import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TasksCreateComponent } from './tasks-create/tasks-create.component';
import { TasksListComponent } from './tasks-list/tasks-list.component';
import { TasksUpdateComponent } from './tasks-update/tasks-update.component';
import { TasksViewComponent } from './tasks-view/tasks-view.component';

const routes: Routes = [
  {
    path: 'tasks-list',
    component: TasksListComponent
  },
  {
    path: 'tasks-create',
    component: TasksCreateComponent
  },
  {
    path: 'tasks-view',
    component: TasksViewComponent
  },
  {
    path: 'tasks-update',
    component: TasksUpdateComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TasksRoutingModule { }
