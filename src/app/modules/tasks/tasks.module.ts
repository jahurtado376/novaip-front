import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TasksRoutingModule } from './tasks-routing.module';
import { TasksViewComponent } from './tasks-view/tasks-view.component';
import { TasksUpdateComponent } from './tasks-update/tasks-update.component';
import { TasksListComponent } from './tasks-list/tasks-list.component';
import { TasksCreateComponent } from './tasks-create/tasks-create.component';


@NgModule({
  declarations: [
    TasksViewComponent,
    TasksUpdateComponent,
    TasksListComponent,
    TasksCreateComponent
  ],
  imports: [
    CommonModule,
    TasksRoutingModule
  ]
})
export class TasksModule { }
