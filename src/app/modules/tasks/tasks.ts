export interface ITasks {
    idTask?: number;
    name?: string;
    description?: string;
    alias?: string;
    state?: boolean;
    deleted?: boolean;
    startDate?: Date;
    endDate?: Date;
    creationDate?: Date;
    updateDate?: Date;
}

export class Tasks implements ITasks {
    constructor(
        idTask: number,
        name: string,
        description: string,
        alias: string,
        state: boolean,
        deleted: boolean,
        startDate: Date,
        endDate: Date,
        creationDate: Date,
        updateDate: Date,
    ){}
}