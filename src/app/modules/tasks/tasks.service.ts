import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { createRequestParams } from 'src/app/utils/utils';
import { environment } from 'src/environments/environment';
import { ITasks } from './tasks';

@Injectable({
  providedIn: 'root'
})
export class TasksService {

  constructor(private http: HttpClient) { }

  public query(req?: any): Observable<ITasks[]>{
    let params = createRequestParams(req);
    return this.http.get<ITasks[]>(`${environment.END_POINT}/api/tasks`, {params})
    .pipe(map(res => {
      return res;
    }));
  }

  public saveProjects(tasks: ITasks): Observable<ITasks>{
    return this.http.post<ITasks>(`${environment.END_POINT}/api/tasks`, tasks)
    .pipe(map(res =>{
      return res;
    }));
  }

  public updateProjects(tasks: ITasks): Observable<ITasks>{
    return this.http.put<ITasks>(`${environment.END_POINT}/api/tasks`, tasks)
    .pipe(map(res =>{
      return res;
    }));
  }

  public deleteProjects(id: string): Observable<ITasks>{
    return this.http.delete<ITasks>(`${environment.END_POINT}/api/tasks/${id}`)
    .pipe(map(res =>{
      return res;
    }));
  }

  public getIdProjects(id: string): Observable<ITasks>{
    return this.http.get<ITasks>(`${environment.END_POINT}/api/tasks/${id}`)
    .pipe(map(res =>{
      return res;
    }));    
  }
}
