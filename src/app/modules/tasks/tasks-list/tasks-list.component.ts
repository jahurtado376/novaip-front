import { Component, OnInit } from '@angular/core';
import { ITasks } from '../tasks';
import { TasksService } from '../tasks.service';

@Component({
  selector: 'app-tasks-list',
  templateUrl: './tasks-list.component.html',
  styleUrls: ['./tasks-list.component.styl']
})
export class TasksListComponent implements OnInit {
  tasksList: ITasks[] = [];

  pageSize = 5;
  pageNumber = 0;

  constructor(private service: TasksService) { }

  ngOnInit(): void {
    this.service.query({pageSize: this.pageSize,
      pageNumber: this.pageNumber})
    .subscribe((res: any) => {
      this.tasksList = res.content
      console.log('res', res);
    })
  }

}
