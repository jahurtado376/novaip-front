import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'projects',
    loadChildren: () => import('./modules/projects/projects.module')
    .then(m => m.ProjectsModule)
  },
  {
    path: 'rols',
    loadChildren: () => import('./modules/rols/rols.module')
    .then(m => m.RolsModule)
  },
  {
    path: 'tasks',
    loadChildren: () => import('./modules/tasks/tasks.module')
    .then(m => m.TasksModule)
  },
  {
    path: 'users',
    loadChildren: () => import('./modules/users/users.module')
    .then(m => m.UsersModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
